(function() {
  'use strict';

  angular
    .module('spa', ['ngMessages', 'ui.router', 'ui.bootstrap']);

})();
